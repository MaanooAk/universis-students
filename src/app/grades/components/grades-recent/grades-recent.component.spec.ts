import {async, ComponentFixture, TestBed} from '@angular/core/testing';
import {TranslateModule} from '@ngx-translate/core';
import {CollapseModule} from 'ngx-bootstrap/collapse';
import {ModalModule} from 'ngx-bootstrap/modal';
import {ErrorService, LocalizedAttributesPipe, ModalService, SharedModule} from '@universis/common';
import {GradesService} from '../../services/grades.service';
import { GradeScaleService, GradeScale } from '@universis/common';
import {HttpClientTestingModule} from '@angular/common/http/testing';
import {AngularDataContext, DATA_CONTEXT_CONFIG, MostModule} from '@themost/angular';
import { ConfigurationService } from '@universis/common';
import {CUSTOM_ELEMENTS_SCHEMA} from '@angular/core';
import {GradesRecentComponent} from './grades-recent.component';
import {TestingConfigurationService} from '../../../test';
import {LoadingService} from '@universis/common';
import {NgChartsModule} from 'ng2-charts';
import {NgPipesModule} from 'ngx-pipes';
import {RouterTestingModule} from '@angular/router/testing';

describe('GradesRecentComponent', () => {
    let component: GradesRecentComponent;
    let fixture: ComponentFixture<GradesRecentComponent>;

  const errorSvc = jasmine.createSpyObj('ErrorService', ['navigateToError']);
  const modalSvc = jasmine.createSpyObj('ModalService' , ['showDialog', 'openModal']);
  const loadingSvc = jasmine.createSpyObj('LoadingService', ['showLoading', 'hideLoading']);

  const gradeSvc = jasmine.createSpyObj('GradesService', ['getAllGrades', 'getCourseTeachers', 'getGradeInfo',
                                          'getDefaultGradeScale', 'getThesisInfo', 'getLastExamPeriod', 'getRecentGrades',
                                          'getGradesSimpleAverage', 'getGradesWeightedAverage']);
    const gradeScaleSvc = jasmine.createSpyObj('GradeScaleService', ['getGradeScales', 'getGradeScale']);

    gradeSvc.getRecentGrades.and.returnValue(Promise.resolve(JSON.parse('[]')));
    const gradeScale = Object.assign(new GradeScale('en'), JSON.parse('{"formatPrecision":2, "scaleType":0, "scaleFactor":1}'));
    gradeSvc.getDefaultGradeScale.and.returnValue(Promise.resolve(gradeScale));

    beforeEach(async(() => {
        return TestBed.configureTestingModule({
            declarations: [GradesRecentComponent],
            providers: [
                {
                    provide: GradesService,
                    useValue: gradeSvc
                },
                {
                    provide: GradeScaleService,
                    useValue: gradeScaleSvc
                },
                {
                    provide: ConfigurationService,
                    useClass: TestingConfigurationService
                },
              {
                provide: LoadingService,
                useValue: loadingSvc
              },
              {
                provide: ModalService,
                useValue: modalSvc
              },
              {
                provide: ErrorService,
                useValue: errorSvc
              },
              LocalizedAttributesPipe
            ],
            imports: [
              RouterTestingModule,
              NgChartsModule,
                TranslateModule.forRoot(),
                MostModule.forRoot({
                    base: '/',
                    options: {
                        useMediaTypeExtensions: false
                    }
                }),
                NgPipesModule,
                CollapseModule,
                HttpClientTestingModule,
                ModalModule.forRoot(),
                SharedModule
            ],
          schemas: [
            CUSTOM_ELEMENTS_SCHEMA
          ]
        }).compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(GradesRecentComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
