import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {AuthGuard} from '@universis/common';
import { GradesRecentComponent } from './components/grades-recent/grades-recent.component';
import { GradesAllComponent } from './components/grades-all/grades-all.component';
import {GradesHomeComponent} from './components/grades-home/grades-home.component';
import {GradesThesesComponent} from './components/grades-theses/grades-theses.component';
import { GradesInternshipComponent } from './components/grades-internship/grades-internship.component';

const routes: Routes = [
    {
        path: '',
        component: GradesHomeComponent,
        canActivate: [
            AuthGuard
        ],
        children: [
            {
                path: '',
                pathMatch: 'full',
                redirectTo: 'recent'
            },
            {
                path: 'all',
                component: GradesAllComponent
            },
            {
                path: 'recent',
                component: GradesRecentComponent
            },
            {
                path: 'theses',
                component: GradesThesesComponent
            },
            {
                path: 'internship',
                component: GradesInternshipComponent
            }
        ]
    }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class GradesRoutingModule { }
