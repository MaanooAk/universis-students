import {Component, Input, OnInit} from '@angular/core';
import { AngularDataContext } from '@themost/angular';
import { ClientDataQueryable } from '@themost/client';
import { ErrorService, LoadingService } from '@universis/common';

@Component({
  selector: 'app-teaching-events',
  templateUrl: './teaching-event.component.html'
})
export class TeachingEventComponent implements OnInit {
  public upcomingEventsQuery: ClientDataQueryable;
  public events: any [] = [];
  public isLoading = true;

  constructor(private _context: AngularDataContext, private _loadingService: LoadingService, private _errorService: ErrorService) {
    this.upcomingEventsQuery = this._context.model('students/me/TeachingEvents')
      .where('eventHoursSpecification').equal(null)
      .and('startDate').greaterOrEqual((new Date()).toISOString())
      .expand('location')
      .take(-1);
  }

  async ngOnInit() {
    this._loadingService.showLoading();
    try {
      let teachingEvents = await this._context.model('students/me/TeachingEvents')
        .select('id,name,startDate,endDate,description,superEvent,eventHoursSpecification,sections,eventStatus,courseClass,performer,location,additionalType')
        .expand('sections', 'eventStatus', 'performer', 'location', 'courseClass($expand=course)', 'eventHoursSpecification')
        .orderBy('startDate')
        .take(-1)
        .getItems();

      let courseExamEvents = await this._context
        .model('students/me/availableCourseExamEvents')
        .select('CourseExamEventSummary')
        .expand('performer, courseExam($expand=course($select=id,displayCode))')
        .take(-1)
        .getItems();

      this.events = teachingEvents.concat(courseExamEvents);

      this.isLoading = false;
      this._loadingService.hideLoading();
    } catch (error) {
      this.isLoading = false;
      this._loadingService.hideLoading();
      this._errorService.navigateToError(error);
    }
  }
}
