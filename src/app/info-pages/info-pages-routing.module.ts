import { NgModule } from '@angular/core';
import {
  Routes,
  RouterModule
} from '@angular/router';


import { DepartmentInfoComponent } from './components/department-info/department-info.component';
import { InfoPagesHomeComponent } from './components/info-pages-home/info-pages-home.component';


const routes: Routes = [
  {
    path: '',
    component: InfoPagesHomeComponent,
    data: {
      title: 'Info Pages'
    },
    children: [
      {
          path: '',
          pathMatch: 'full',
          redirectTo: 'department'
      },
      {
          path: 'department',
          component: DepartmentInfoComponent
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class InfoPagesRoutingModule { }
