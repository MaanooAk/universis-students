import { NgModule } from '@angular/core';
import {
  Routes,
  RouterModule
} from '@angular/router';


import { ConsentsHomeComponent } from './components/consents-home/consents-home.component';
import { ConsentGroupComponent } from './components/consent-group/consent-group.component';


const routes: Routes = [
  {
    path: '',
    component: ConsentsHomeComponent,
    data: {
      title: 'Consents'
    },
    children: [
      {
        path: '',
        component: ConsentGroupComponent
      },
      {
        path: ':group',
        component: ConsentGroupComponent
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ConsentsRoutingModule { }
