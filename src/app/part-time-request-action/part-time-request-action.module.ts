import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PartTimeRequestActionRoutingModule } from './part-time-request-action-routing.module';
import { PreviewComponent } from './preview/preview.component';
import { FormsModule } from '@angular/forms';
import { MostModule } from '@themost/angular';
import { SharedModule } from '@universis/common';
import { TranslateModule, TranslateService } from '@ngx-translate/core';
import { AdvancedFormsModule } from '@universis/forms';


import * as el from "./i18n/part-time-request-action.el.json"
import * as en from "./i18n/part-time-request-action.en.json"
import {RequestsSharedModule} from "../requests/requests-shared.module";
import {TabsModule} from "ngx-bootstrap/tabs";
import {NewComponent} from "./apply/new.component";
import {NgPipesModule} from "ngx-pipes";
import {NgxDropzoneModule} from "ngx-dropzone";

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    MostModule,
    SharedModule,
    TranslateModule,
    AdvancedFormsModule,
    PartTimeRequestActionRoutingModule,
    RequestsSharedModule,
    TabsModule,
    NgPipesModule,
    NgxDropzoneModule
  ],
  declarations: [PreviewComponent,NewComponent]
})
export class PartTimeRequestActionModule {

  constructor(private _translateService: TranslateService) {
    this.ngOnInit().catch( err => {
      console.error('An error occurred while loading PartTimeRequestActionModule.');
      console.error(err);
    });
  }

  // tslint:disable-next-line: use-life-cycle-interface
  async ngOnInit() {
    this._translateService.setTranslation("el", el, true);
    this._translateService.setTranslation("en", en, true);
  }

 }
