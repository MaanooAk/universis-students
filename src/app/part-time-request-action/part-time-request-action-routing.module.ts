import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PreviewComponent } from './preview/preview.component';
import {NewComponent} from "./apply/new.component";

const routes: Routes = [
  {
    path: 'apply',
    canActivate: [
    ],
    component: NewComponent,
    data: {
      title: 'Apply'
    }
  },
  {
    path: ':code/preview',
    component: PreviewComponent
  }
];

@NgModule({
  imports: [
    RouterModule.forChild(routes)
  ],
  exports: [RouterModule]
})
export class PartTimeRequestActionRoutingModule { }
