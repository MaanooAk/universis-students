import { Component, OnInit, Input , ViewChild, Output} from '@angular/core';
import {AngularDataContext} from '@themost/angular';
import {ConfigurationService, GradeScale, ModalService} from '@universis/common';
import { ElementRef, TemplateRef } from '@angular/core';
import {RequestsService} from '../../services/requests.service';
import {MessageSharedService} from '../../../students-shared/services/messages.service';
import { LoadingService } from '@universis/common';
import {TranslateService} from '@ngx-translate/core';
import {ProfileService} from '../../../profile/services/profile.service';
import { Router} from '@angular/router';
import {ErrorService} from '@universis/common';

@Component({
  selector: 'app-auto-request',
  templateUrl: './auto-request.component.html',
  styleUrls: ['./auto-request.component.scss']
})
export class AutoRequestComponent implements OnInit {
  @ViewChild('templateSendRequest') sendRequestTemplate?: TemplateRef<any>;
  @ViewChild('templateFail') failTemplate?: TemplateRef<any>;

  @Input() public requestType: any;
  @Input() public url: any;

  public checkRequest: any;
  public documentRequests: any = [];
  public availableRequestType  = false;
  private modalRef: any;
  public isLoading = true;
  public hasError = false;
  public foundRequestType?: boolean;
  public foundReport?: boolean;

  constructor(private requestsService: RequestsService,
              private modalService: ModalService,
              private _context: AngularDataContext,
              private loadingService: LoadingService,
              private translate: TranslateService,
              private _profileService: ProfileService,
              private _router: Router,
              private messageSharedService: MessageSharedService,
              private _errorService: ErrorService,
              private _configuration: ConfigurationService) { }

  async ngOnInit() {
    this.checkRequest = await this._context.model('DocumentConfigurations')
      .asQueryable()
      .where('inLanguage')
      .equal(this._configuration.currentLocale)
      .and('alternateName').equal(this.requestType)
      .getItem();
    this.foundRequestType = this.checkRequest != null;
    if (!this.foundRequestType) {
      const reportTypes = await this.requestsService.getReportTemplates();
      this.checkRequest = reportTypes.find(x => {
        return x.alternateName === this.requestType;
      });
      if (this.checkRequest) {
        this.foundReport = true;
      }
    }
    if (this.checkRequest && (this.foundReport || this.foundRequestType)) {
      this.availableRequestType = true;
    }
    // this.loadingService.hideLoading();
    this.isLoading = false;
  }

  sendRequest(title: string) {
    this.loadingService.showLoading();
    this._context.model('RequestDocumentActions').save({
      alternateName: title,
      object: {
        alternateName: title
      },
    })
      .then(request => {
        this.isLoading = true;
        this.hasError = false;
        this.loadingService.hideLoading();
        this.requestsService.getDocumentRequest(request.id).then((documentRequests) => {
          this.documentRequests = documentRequests;
          if (this.documentRequests.messages.length > 0) {
            this.modalService.showDialog(
              this.translate.instant('NewRequestTemplates.' + title + '.Title'),
              this.translate.instant(this.documentRequests.messages[0].body)
            ).then(() => {
              this.messageSharedService.callUnReadMessages();
// navigate to other url only if request is completed
              if (this.documentRequests.actionStatus.alternateName === 'CompletedActionStatus') {
                return this._router.navigate([this.url]);
              }
            });
          }
        });
      }).catch((err) => {
      this.hasError = true;
      this.isLoading = false;
      this.loadingService.hideLoading();
      this.modalRef = this.modalService.openModal(this.failTemplate, 'modal-lg');
    });
  }
  openModal(template: TemplateRef<any>) {
    this.modalRef = this.modalService.openModal(template, 'modal-md');
  }

  closeModal() {
    // Reload the page. A more specific function may be implemented later
    this.ngOnInit();
    this.modalRef.hide();
  }

  async printReport(reportName) {
    this.loadingService.showLoading();
    let templates;
    try {
      templates = await this.requestsService.getReportTemplates();
    } catch (err) {
      this._errorService.showError(err);
    }
    if (templates && Array.isArray(templates)) {
      let template = templates.find(x => {
        return x.alternateName === reportName;
      });
      if (template) {
        const student = await this._profileService.getStudent();
        try {
          await this.requestsService.printReport(template.id, {
            ID: student.id,
            REPORT_USE_DOCUMENT_NUMBER: false
          }, `${student.person.givenName} ${student.person.familyName} - ${reportName}`);
        } catch (err) {
          this._errorService.showError(err);
        }
      } else {
        this._errorService.showError(new Error('Couldn\'t find a matching report template.'), {
          continueLink: '.',
          iconClass: 'fa fa-times-circle'
        });
      }
    }
    this.loadingService.hideLoading();
  }
}
