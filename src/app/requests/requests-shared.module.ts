import { ModuleWithProviders, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MostModule } from '@themost/angular';
import { RequestsService } from './services/requests.service';
import { ProfileSharedModule } from '../profile/profile-shared.module';
import { AutoRequestComponent } from './components/auto-request/auto-request.component';
import { TranslateModule, TranslateService } from '@ngx-translate/core';
import { environment } from '../../environments/environment';
import { TooltipModule } from 'ngx-bootstrap/tooltip';

import * as el from "./i18n/requests.el.json"
import * as en from "./i18n/requests.en.json"
import {DocumentSubmissionComponent} from "./components/document-submission/document-submission.component";
import {NgPipesModule} from "ngx-pipes";
import {NgxDropzoneModule} from "ngx-dropzone";

@NgModule({
  imports: [
    CommonModule,
    MostModule,
    TranslateModule,
    ProfileSharedModule,
    TooltipModule,
    NgPipesModule,
    NgxDropzoneModule
  ],
  declarations: [
    AutoRequestComponent,
    DocumentSubmissionComponent
  ],
  exports: [
    AutoRequestComponent,
    DocumentSubmissionComponent
  ],
})

export class RequestsSharedModule {
  constructor(private _translateService: TranslateService) {
    this._translateService.setTranslation("el", el, true);
    this._translateService.setTranslation("en", en, true);
  }
  static forRoot(): ModuleWithProviders<RequestsSharedModule> {
    return {
      ngModule: RequestsSharedModule,
      providers: [
        RequestsService
      ]
    };
  }
}
