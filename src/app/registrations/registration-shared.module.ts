import {ModuleWithProviders, NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {MostModule} from '@themost/angular';
import {CurrentRegistrationService} from './services/currentRegistrationService.service';
import {ProfileSharedModule} from '../profile/profile-shared.module';

@NgModule({
  imports: [
    CommonModule,
    MostModule,
    ProfileSharedModule
  ]
})

export class RegistrationSharedModule {
  constructor() {}
  static forRoot(): ModuleWithProviders<RegistrationSharedModule> {
    return {
      ngModule: RegistrationSharedModule,
      providers: [
        CurrentRegistrationService
        ]
    };
  }
}
