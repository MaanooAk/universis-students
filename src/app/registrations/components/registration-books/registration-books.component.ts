import { Component } from '@angular/core';
import { AngularDataContext } from '@themost/angular';
import { ConfigurationService, LoadingService, ErrorService } from '@universis/common';
import { BookDeliveriesService } from '../../services/book-deliveries.service';

@Component({
  selector: 'app-registration-books',
  templateUrl: './registration-books.component.html',
  styleUrls: ['./registration-books.component.scss']
})
export class RegistrationBooksComponent {
  public loading = true;
  public registrations: any = [];
  public currentLanguage;
  public allRegistrations: any;
  private period: any;
  public periods: any = [];
  private chosenPeriod: any;
  public defaultLanguage: string | any = "";
  public eudoxusUrl: string;


  constructor(private context: AngularDataContext,
    private _configurationService: ConfigurationService,
    private loadingService: LoadingService,
    private _errorService: ErrorService,
    private _bookDeliveriesService: BookDeliveriesService) {
    this.currentLanguage = this._configurationService.currentLocale;
    this.defaultLanguage = this._configurationService?.settings?.i18n?.defaultLocale;
    this.eudoxusUrl = ((this._configurationService?.settings) as any)?.eudoxus?.url;
  }

  ngOnInit() {
    this.loadingService.showLoading();
    this.context.model('students/me/registrations').asQueryable()
      .expand('classes($orderby=semester,course/displayCode;$expand=course($expand=locale),' +
        'courseClass($expand=instructors($expand=instructor($select=InstructorSummary))),courseType($expand=locale))')
      .orderBy('registrationYear desc')
      .thenBy('registrationPeriod desc')
      .getItems().then(async (res) => {
        this.registrations = this.allRegistrations = res;
        this.initializePeriods();
        for (let i = 0; i < this.registrations.length; i++) {
          const booksResponse = await this._bookDeliveriesService.fetchRegistrationBooks(this.registrations[i].registrationYear.id, this.registrations[i].registrationPeriod.id);
          Object.assign(this.registrations[i], booksResponse);
          if (typeof this.registrations[i].bookServiceError == 'undefined') {
            this.registrations[i].bookServiceError = false;
          }
          if (booksResponse.booksFound === true) {
            for (let j = 0; j < this.registrations[i].classes.length; j++) {
              const book = booksResponse.books.find(el => el.courceCode === this.registrations[i].classes[j].course.displayCode);
              if (book) {
                this.registrations[i].classes[j].bookDetails = book;
              } else {
                this.registrations[i].classes[j].bookDetails = null;
              }
            }
          }
        }
        this.loadingService.hideLoading();
        this.loading = false;
      }).catch(err => {
        this.loadingService.hideLoading();
        throw err;
      });

  }

  // Initialize periods for the left menu in order to avoid double period names in the list
  initializePeriods() {
    this.registrations.forEach(registration => {
      // if period is not already in the array, push it
      if (!this.periods.includes(registration.registrationYear.alternateName)) {
        this.periods.push(registration.registrationYear.alternateName);
      }
    });
  }

  filterByPeriod(period) {
    this.registrations = this.allRegistrations.filter(x => {
      return x.registrationYear.alternateName === period;
    });
    this.chosenPeriod = period;
  }

  removeSemesterFiltering() {
    this.registrations = this.allRegistrations;
    this.chosenPeriod = '';
  }
}

