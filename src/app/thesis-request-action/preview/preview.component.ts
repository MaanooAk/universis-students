import { Component, OnInit, OnDestroy } from '@angular/core';
import { AngularDataContext } from '@themost/angular';
import {ErrorService, ModalService, LoadingService} from '@universis/common';
import { TranslateService } from '@ngx-translate/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs';
import {ProfileService} from '../../profile/services/profile.service';


@Component({
  selector: 'app-preview',
  templateUrl: './preview.component.html'
})
export class PreviewComponent implements OnInit, OnDestroy {

  public subscription?: Subscription;
  public data: any;
  public department: any;
  public loading = true;
  public thesisRequestActions: any;

  constructor(private _context: AngularDataContext,
    private _errorService: ErrorService,
    private _translateService: TranslateService,
    private _modalService: ModalService,
    private _router: Router,
    private _profileService: ProfileService,
    private _loadingService: LoadingService,
    private _activatedRoute: ActivatedRoute) { }

  showLoading(loading: boolean) {
    this.loading = loading;
    if (loading) {
      this._loadingService.showLoading();
    } else {
      this._loadingService.hideLoading();
    }
  }

  ngOnDestroy(): void {
    if (this.subscription) {
      this.subscription.unsubscribe();
    }
  }

  async ngOnInit() {
    this.showLoading(true);
    this.thesisRequestActions = await this._context.model('StudentRequestActions')
      .asQueryable()
      .where('additionalType').equal('ThesisRequestAction')
      .expand('actionStatus, messages($orderby=dateCreated desc;$expand=attachments)')
      .orderByDescending('dateCreated')
      .take(-1)
      .getItems();
    this.showLoading(false);
  }

}
