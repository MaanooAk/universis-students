import { TestBed } from '@angular/core/testing';

import { ThesisProposalsService } from './thesis-proposals.service';

describe('ThesisProposalsService', () => {
  let service: ThesisProposalsService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ThesisProposalsService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
