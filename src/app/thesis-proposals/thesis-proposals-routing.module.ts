import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuard } from '@universis/common';
import { ThesisProposalsListComponent } from './thesis-proposals-list/thesis-proposals-list.component';
import { TranslateService } from '@ngx-translate/core';

import * as el from "./i18n/theses.el.json"
import * as en from "./i18n/theses.en.json"
import { ThesisProposalComponent } from './thesis-proposal/thesis-proposal.component';

const routes: Routes = [
  {
      path: '',
      canActivate: [
          AuthGuard
      ],
      children: [
          {
              path: '',
              pathMatch: 'full',
              redirectTo: 'browse'
          },
          {
              path: 'browse',
              component: ThesisProposalsListComponent
          },
          
      ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ThesisProposalsRoutingModule {

  constructor(private _translateService: TranslateService) {
      this._translateService.setTranslation("el", el, true);
      this._translateService.setTranslation("en", en, true);
  }
}