import { AfterViewInit, Component, OnDestroy } from '@angular/core';
import { ThesisProposalsService } from '../thesis-proposals.service';
import { LoadingService, ErrorService } from '@universis/common';
import { ProfileService } from 'src/app/profile/services/profile.service';
import { Subscription } from 'rxjs';
import { ActivatedRoute, Router } from '@angular/router';
import { animate, keyframes, state, style, transition, trigger } from '@angular/animations';
import { AngularDataContext } from '@themost/angular';

@Component({
  selector: 'app-thesis-proposals-list',
  templateUrl: './thesis-proposals-list.component.html',
  styleUrls: ['./thesis-proposals-list.component.scss'],
  animations: [
    trigger('showList', [
      state('hide', style({
        display: 'none'
      })),
      transition('show => hide', [
        animate('0.2s', keyframes([
          style({ width: '400px', opacity: 0.8 }),
          style({ width: '300px', opacity: 0.7 }),
          style({ width: '200px', opacity: 0.6 }),
          style({ width: '100px', opacity: 0.3 }),
          style({ width: '0px', opacity: 0 })
        ]))
      ])
    ])
  ]
})
export class ThesisProposalsListComponent implements AfterViewInit, OnDestroy {

  public thesisProposals: any = [];
  public isLoading = false;
  public count: any;
  public searchText = '';
  public selectedThesisType = null;
  public selectedThesisStatus = null;
  public select: any = [];
  public student: any;
  private studentUser!: number;
  public thesisProposal: any;
  public singleThesis: any = null;
  private toggleShowListSubscription!: Subscription;
  private paramSubscription!: Subscription;
  public showList: boolean = true;
  public showSingle!: boolean;
  public thesisRequestActions: any;
  public studentState: any;
  private currentYear: any;

  constructor(private _thesisProposalService: ThesisProposalsService,
    private _context: AngularDataContext,
    private _loadingService: LoadingService,
    private _errorService: ErrorService,
    private _profileService: ProfileService, 
    private _router: Router,
    private _activatedRoute: ActivatedRoute ) { }


  async ngAfterViewInit() {
    this._loadingService.showLoading();
    this.toggleShowListSubscription = this._thesisProposalService.toggleShow.subscribe((val) => this.showList = val);
    this.student = await this._profileService.getStudent();
    if (this.student) {
      this.studentState = await this._thesisProposalService.validateStudent(this.student);
      if (this.studentState.valid === true && this.studentState.maximumAllowedThesis != null) {
        this.isLoading = true;
        this.studentUser = this.student.user.id;
        this.currentYear = this.student.department && this.student.department.currentYear && this.student.department.currentYear.id;
        await this._thesisProposalService.getThesisProposals(this.currentYear).then(async (res: any) => {
          // the method enhances the object with properties related to the UI
          this.thesisProposals = await this.injectUIProperties(res); 
          this.paramSubscription = this._activatedRoute.fragment.subscribe(fragment => { 
            if (fragment) {
              this.singleThesis = this.thesisProposals.find(el => el.id == fragment);
              this.showSingleThesisProposal(this.singleThesis);
            }  else {
              this._thesisProposalService.updateShowStatus(true);
              this.isLoading = false;
              this._loadingService.hideLoading();
            }});
        }).catch(err => {
          console.log(err)
          // hide loading
          this._loadingService.hideLoading();
          return this._errorService.navigateToError(err);
        });
      } else {
        this.isLoading = false;
        this._loadingService.hideLoading();
      }
    }
  }

  // enhances the object with properties related to the UI
  async injectUIProperties(res: any) {
    this.thesisRequestActions = await this._context.model('ThesisRequestActions').select('thesisProposal', 'actionStatus').getItems();
    const updatedTheses = await Promise.all(res.map(async proposal => {
        Object.assign(proposal, {
            STUDENT_INTERESTED: false,
            request: {},
            INSTRUCTOR_ACCEPTED: false,
            STUDENT_ACCEPTED: false,
            STUDENT_REJECTED: false,
            INSTRUCTOR_REJECTED: false
        });

        if (proposal.actions && proposal.actions.length > 0) {
            const request = this.thesisRequestActions.find(el => el.thesisProposal === proposal.id);
            if (request) {
              proposal.request = { actionStatus: request.actionStatus ? request.actionStatus.alternateName : null };
            }

            proposal.INSTRUCTOR_ACCEPTED = !!proposal.actions.some(el =>
                el.actionStatus && el.actionStatus.alternateName === 'CompletedActionStatus' && el.owner !== this.studentUser
            );
            proposal.INSTRUCTOR_REJECTED = !!proposal.actions.some(el =>
                el.actionStatus && el.actionStatus.alternateName === 'CancelledActionStatus' && el.owner !== this.studentUser && el.modifiedBy !== this.studentUser
            );
            proposal.STUDENT_INTERESTED = !!proposal.actions.some(el =>
                el.actionStatus && ['ActiveActionStatus', 'CompletedActionStatus','CancelledActionStatus'].includes(el.actionStatus.alternateName) && el.owner !== this.studentUser
            );
            proposal.STUDENT_REJECTED = !!proposal.actions.some(el =>
                el.actionStatus && el.actionStatus.alternateName === 'CancelledActionStatus' && el.modifiedBy === this.studentUser
            );
            proposal.STUDENT_ACCEPTED = !!proposal.actions.some(el =>
                el.actionStatus && el.actionStatus.alternateName === 'CompletedActionStatus' && el.owner === this.studentUser
            );

            proposal.STUDENT_ACCEPTED = !!proposal.actions.some(el =>
              el.actionStatus && el.actionStatus.alternateName === 'CompletedActionStatus' && el.owner === this.studentUser
            );
            proposal.THESIS_ASSIGNED_TO_STUDENT = !!(proposal.STUDENT_ACCEPTED && proposal.request && proposal.request.actionStatus === 'CompletedActionStatus' && proposal.thesis
                                                  && await this._context.model('students/me/theses').where('thesis').equal(proposal.thesis).getItem());
        }
        proposal.student = this.studentUser;
        return proposal;
    }));

    // Check if student reached studyProgram's thesisRequired
    let filtered = updatedTheses;
    const thesisFound = updatedTheses.filter(el => el.THESIS_ASSIGNED_TO_STUDENT === true);
    if (thesisFound && thesisFound.length > 0 && !(thesisFound.length < this.studentState.maximumAllowedThesis)) {
        if (thesisFound.length <= this.studentState.maximumAllowedThesis ) {
            filtered = updatedTheses.filter(el => el.actions.length !== 0);
        } else {
          // it will not be needed, but just in case it is
          this._loadingService.hideLoading();
          console.log('Maximum number of allowed thesis exceeded');
            return this._errorService.navigateToError('error');
        }
    } 

    // Filter: proposals not assigned yet that a. without interaction b. that have expressed interest c. that instructor has accepted or rejected
    const result = filtered.filter(el =>
        (el.actions.length === 0 && el.status.alternateName === 'potential') ||
        el.STUDENT_INTERESTED ||
        el.STUDENT_REJECTED ||
        el.INSTRUCTOR_ACCEPTED ||
        el.INSTRUCTOR_REJECTED
    );
    return result;
}


  search() {
    const query = this._thesisProposalService.searchThesisProposals(this.searchText, this.currentYear);
    if (this.selectedThesisType) {
      query.and('type/alternateName').equal(this.selectedThesisType);
    }
    try {
      this.isLoading = true;
      this._loadingService.showLoading();
      query.getItems().then(async (res: any) => {
        this.thesisProposals = await this.injectUIProperties(res)
        this.count = res.length;
        this.isLoading = false;
        this._loadingService.hideLoading();
      }).catch(err => {
        // hide loading
        this._loadingService.hideLoading();
        return this._errorService.navigateToError(err);
      });
    } catch (err) {
      this._loadingService.hideLoading();
      console.error(err);
    }
  }

 toggleMore(i: number, thesisProposalID: number) {
  this.thesisProposal = this.thesisProposals.filter((el) => el.id === thesisProposalID ? el : null)[0];
    if (this.thesisProposal.show && this.thesisProposal.show === true) {
      this.thesisProposal.show = !this.thesisProposal.show;
    } else {
      this.thesisProposal = this.thesisProposals.filter((el) => el.id === thesisProposalID ? el : null)[0];
      Object.assign(this.thesisProposal, { 'show': true });
    }
  }


  showSingleThesisProposal(dataSingle: any) {
    if (this.studentState.maximumAllowedThesis != null) {
      this.showList = false;
      this._router.navigate( [ '/thesis-proposals/browse' ], { fragment: `${dataSingle.id}` } );
      this.singleThesis = dataSingle;
      this.isLoading = false;
      this._loadingService.hideLoading();
    }
  }

  ngOnDestroy() {
    if (this.toggleShowListSubscription) {
      this.toggleShowListSubscription.unsubscribe();
    }
    if (this.paramSubscription) {
      this.paramSubscription.unsubscribe();
    }
  }


  async updateThesisProposalList(event: any) {
    if (event) {
      await this._thesisProposalService.getThesisProposals(this.student.department.currentYear.id).then(async (res: any) => {
        this.thesisProposals = await this.injectUIProperties(res);
      });
    }
  }

}
