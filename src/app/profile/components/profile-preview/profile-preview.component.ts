import {Component, OnInit} from '@angular/core';
import { UserService } from '@universis/common';
import { ProfileService } from '../../services/profile.service';
import { LoadingService } from '@universis/common';
import {AngularDataContext} from '@themost/angular';
import {ErrorService} from '@universis/common';

@Component({
    selector: 'app-profile-preview',
    templateUrl: 'profile-preview.component.html',
    styleUrls: ['profile-preview.component.scss']
})

export class ProfilePreviewComponent implements OnInit {

    // student object coming from the API
    public student: any;
    public loading = true;   // Only if data is loaded
    public programGroups;
    public studentCounselors?: Array<any>;
    public studentCounselorConcent?: boolean;

    constructor(private _userService: UserService,
        private _profileService: ProfileService,
        private _context: AngularDataContext,
        private loadingService: LoadingService,
        private _errorService: ErrorService) {
    }

    async ngOnInit() {
      this.loadingService.showLoading();  // show loading
      try {
        const res = await this._profileService.getStudent();
        // get program groups
        this.programGroups = await this.getProgramGroups();
        this.studentCounselors = await this.getCounselors();
        this.studentCounselorConcent = await this.hasCounselorConcent();
        this.student = res; // Load data
        this.loading = false; // Data is loaded
        this.loadingService.hideLoading(); // hide loading
      } catch (err) {
          this.loading = false; // Data is loaded
          this.loadingService.hideLoading(); // hide loading
          return this._errorService.navigateToError(err);
      }
   }

  async getProgramGroups() {
    return this._context.model('students/me/programGroups').asQueryable().expand('programGroup').getItems();
  }

  async getCounselors() {
    return this._context.model('students/me/counselors').asQueryable().expand('instructor($select=InstructorSummary)')
      .where('active').equal(true).getItems();
  }

  async hasCounselorConcent() {
    const consents = await this._context.model("users/me/consents").getItems();
    const value = consents?.value["consultationStudent"]?.["consultationStudentGeneralAccess"].val;
    return value == "y";
  }
}
