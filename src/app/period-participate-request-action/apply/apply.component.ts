import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { AngularDataContext } from '@themost/angular';
import { ErrorService, ModalService,  DIALOG_BUTTONS, LoadingService, ConfigurationService } from '@universis/common';
import { TranslateService } from '@ngx-translate/core';
import { Router } from '@angular/router';
import { ProfileService } from 'src/app/profile/services/profile.service';
import { cloneDeep } from 'lodash';
import { ToastrService } from 'ngx-toastr';
import { HttpClient } from '@angular/common/http';
import { Converter } from 'showdown';
import {RequestsService} from "../../requests/services/requests.service";

@Component({
  selector: 'app-apply',
  templateUrl: './apply.component.html'
})
export class ApplyComponent implements OnInit {

    public data: any;
    public department: any;
    public student: any;
    public loading = true;
    public notAllowed = false;
    public terms: any;


    constructor(private _context: AngularDataContext,
                private _errorService: ErrorService,
                private _translateService: TranslateService,
                private _modalService: ModalService,
                private _router: Router,
                private _toastService: ToastrService,
                private _loadingService: LoadingService,
                private _profileService: ProfileService,
                private _http: HttpClient,
                private _configurationService: ConfigurationService,
                private requestsService: RequestsService) {
    }

    showLoading(loading: boolean) {
        this.loading = loading;
        if (loading) {
            this._loadingService.showLoading();
        } else {
            this._loadingService.hideLoading();
        }
    }

    async ngOnInit() {
        try {
            this.showLoading(true);
            // get department data
            this.department = await this._context.model('students/me/department')
                .asQueryable()
                .expand('currentYear,currentPeriod')
                .getItem();
            this.student = await (await this._profileService.getStudent());
            // validate that request does not exist
            const periodParticipateAction = await this._context.model('PeriodParticipateRequestActions')
                .where('student').equal(this.student.id)
                .and('year').equal(this.department.currentYear.id)
                .and('period').equal(this.department.currentPeriod.id)
                .select('id')
                .getItem();
            if (periodParticipateAction != null) {
                return this._router.navigate(['/requests', 'PeriodParticipateRequestActions', periodParticipateAction.id, 'preview']);
            }
            if (this.student && this.student.studentStatus && this.student.studentStatus.alternateName !== 'active') {
                // exit
                this.data = {};
                this.showLoading(false);
                return;
            }

            // validate student against request
            const requestType = await this._context.model('StudentRequestConfigurations')
                .asQueryable()
                .where('additionalType')
                .equal('PeriodParticipateRequestAction')
                .getItem();
            // clone requestType
            if (!requestType ||
                (requestType && !requestType.validationResult)
                || (requestType && requestType.validationResult && requestType.validationResult.success!==true)
            ) {
               this.notAllowed = true;
               this.showLoading(false);
               return;
            }

            // get current language
            const currentLocale = this._configurationService.currentLocale;
            // get md file if any;
            const termsFile = `assets/docs/PeriodParticipateRequestAction.terms.${currentLocale}.md`
            this._http.get(termsFile, {responseType: 'text'}).subscribe((terms) => {
                this.terms = new Converter({tables: true}).makeHtml(terms);
            });
            // set agree to false (leave user to select)
            const agree = false;
            // set reject to false (leave user to select)
            const reject = false;

            // get academic year
            const year = this.department.currentYear;
            // get period
            const period = this.department.currentPeriod;
            // set data
            this.data = {
                agree,
                reject,
                year,
                period
            };
            this.showLoading(false);
        } catch (err) {
            this.showLoading(false);
            this._errorService.showError(err);
        }
    }

    submit(form: NgForm) {
        // get confirm message for accept or reject
        let ConfirmMessage: string = "";
        const ConfirmMessageTitle = this._translateService.instant('PeriodParticipateRequestActions.Title.One');
        if (this.data.agree) {
            ConfirmMessage = this._translateService.instant('PeriodParticipateRequestActions.ConfirmMessage.Accept', this.data);
        } else if (this.data.reject) {
            ConfirmMessage = this._translateService.instant('PeriodParticipateRequestActions.ConfirmMessage.Reject');
        }
        // show dialog
        this._modalService.showDialog(ConfirmMessageTitle, ConfirmMessage, DIALOG_BUTTONS.OkCancel).then(result => {
            if (result === 'ok') {
                this.showLoading(true);
                const submissionData = cloneDeep(this.data);
                // format payload
                submissionData.agree = submissionData.agree ? 1 : 0;
                submissionData.year = submissionData.year.id;
                submissionData.period = submissionData.period.id;
                submissionData.body = submissionData.agree ?
                    `${this._translateService.instant('PeriodParticipateRequestActions.MessageBodyAgree')}<br>${this.terms}`
                    : this._translateService.instant('PeriodParticipateRequestActions.MessageBodyNotAgree');
                this._context.model('PeriodParticipateRequestActions')
                    .save(submissionData).then(() => {
                    // get complete message
                    const CompleteMessage = this._translateService.instant('PeriodParticipateRequestActions.CompleteMessage');
                    // hide loading
                    this.showLoading(false);
                    // show toast message
                    this._toastService.success(CompleteMessage, ConfirmMessageTitle,
                        {timeOut: 3000, positionClass: 'toast-top-center', progressBar: false});
                    // navigation to messages
                    this._router.navigate(['/messages']);
                }).catch(err => {
                    this.showLoading(false);
                    this._errorService.showError(err, {
                        continueLink: '.'
                    });
                });
            }
        });
    }

    onAgreeChange() {
        if (this.data.agree === true) {
            this.data.reject = false;
        }

    }
    onRejectChange() {
        if (this.data.reject === true) {
            this.data.agree = false;
        }

    }
}
