CHANGELOG
=========

v1.1.0
------

2019-08-07

* Theme integration
* Add Heroku Review Apps


v1.0.0
------

2019-06-12

* Initial release of Students Application
